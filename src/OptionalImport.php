<?php

namespace Drupal\libraries_optional_import;

use Drupal\Core\Extension\ThemeExtensionList;

final class OptionalImport {

  public function __construct(private readonly ThemeExtensionList $themeExtensionList) {}

  public function optionalJsScripts(array &$libraries, string $extension): void {
    foreach ($libraries as $library_name => $library) {
      if (!isset($library['js'])) {
        continue;
      }

      $libraries[$library_name]['js'] = $this->deleteOptionalUnexistingAssets(
        $library['js'],
        $extension
      );
    }
  }

  public function optionalCssScripts(array &$libraries, string $extension): void {
    foreach ($libraries as $library_name => $library) {
      if (!isset($library['css'])) {
        continue;
      }

      foreach ($library['css'] as $key => $value) {
        $libraries[$library_name]['css'][$key] = $this->deleteOptionalUnexistingAssets(
          $library['css'][$key],
          $extension
        );
      }
    }
  }

  private function deleteOptionalUnexistingAssets(array $assets, string $extension): ?array {
    $theme_path = $this->getThemePath($extension);

    foreach ($assets as $asset_source => $options) {
      if (!isset($options['optional']) || !$options['optional']) {
        continue;
      }

      if (!file_exists("$theme_path/$asset_source")) {
        unset($assets[$asset_source]);
      }
    }

    return $assets;
  }

  private function getThemePath(string $extension): ?string {
    return $this->themeExtensionList->exists($extension)
      ? realpath($this->themeExtensionList->getPath($extension))
      : NULL;
  }

}
