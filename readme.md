# Usage

Add the optional library to the library definition in your module's `libraries.yml` file as shown below

```yaml
global:
  version: 1.x
  css:
    theme:
      dist/app.css: { }
      dist/app-optional.css: { optional: true }
  js:
    dist/app.js: { }
    dist/app-optional.js: { optional: true }
  dependencies:
    - core/jquery
    - core/drupal
    - core/drupalSettings
    - core/once
```
